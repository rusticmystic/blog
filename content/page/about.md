---
title: About me
subtitle: A Brief Intro
comments: false
---

I go by the name **Rustic Mystic** on the web. I have tried blogging on the following links too. 

> [Weebly](http://rusticmystic.weebly.com)
>
> [Blogspot](http://therusticmystic.blogspot.com)

This is my first static site hosted on [gitlab](www.gitlab.com/rusticmystic).
I like to read and write poems, anecdotes and articles. Though they tend to end up mostly in the drafts or notes. With a static site here, i will try that
they they too see the sun.

I am an engineer for the most part of the day. Hailing from India I have an exposure to many languages. Apart from **_English_** I write in **_Hindi_, _Punjabi_, _Urdu_**.
