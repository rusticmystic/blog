---
title: Shikwa
subtitle: A Complaint
date: 2010-02-14 
tags: ["poem", "urdu"]
---

A light _romantic_ verse with **English** _translation_.

<!--more-->

## Shikwa

> yoon umadh ghtaa,jab neelamber pe chhaa gyi,<br/>
>  guzrey zamaanon ki ek baat yaad aa gyi.

*When the black clouds rose in the sky, <br/> that dark ambience made me
remember an event form the past.*

> kaun thhey hum, kaun ho ab gaye hain,<br/>
> kaunsi blaa hamari hasti mitaa gayi.

*What I used to be and What I am now,<br/> What was that damn cause
responsible for annihilation of my identity?*

> Ik arsey se thama rakhi yaad jo dil mein,<br/>
> kyu ab humse yeh shairi krwaa gayi.

*Since ages I had buried your thoughts deep in my heart,<br/> Why is it now
that they erupt as poetry?*

> kyu sukoon si woh saawan ki baad-e-swa,<br/>
> yoon ban aaj dasht ki zalim kzaa gyi.

*Why that soothing cool breeze of the rainy season has transmuted into
rash gush,<br/> as in the autumn in a dry desert?*

> jawani dhali bina machhli koi pakde,<br/>
> pakeezgi k daawon mein buzurgi bharmaa gyi.

*Because young age has passed without flirting with maidens,<br/> now this
old age boasts of that piety.*


> chalo jo bhi hua sab quboola adab se,<br/>
> par gaya na mann se shikwa yahi ki,

*I acknowledge everything that happened in the past as 'good', ( I bow
to Providence in gratitude),<br/> except one complaint, to the Providence,
which still hounds my mind.*


> udte rhe sab parinde hawa mein,<br/>
> bas koyal hamari hi pinjre ko bhaa gyi.

*When all birds were free soaring high in the skies,<br/> why was that only
my beloved nightingale chosen for the 'cage'?*

