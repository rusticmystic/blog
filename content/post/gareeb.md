---
title: Gareeb
subtitle: Another perspective of Wealth
date: 2012-07-20 
tags: ["poem", "urdu"]
---

<!--more-->

_ashq se bhare hain nayan,_<br/>
_labon pe fir bhi aah nhi,_

> The eyes are wet upto the brim, <br/> even then the lips don't move to convey the anguish.

_zalim ye ishq aur shauq ke_<br/>
_toh jalwe bhi ajeeb hain._

> The Show of love and passion is so strange.

_yoon toh tumse meelon ka fasla sahi,_<br/>
_dil-e-nadaan ke tu ab bhi kareeb hai._

> Though you are distant from me in miles, <br/>but this ignorant heart always feels you near.

_har dam mein tu, harf-e-dua mein bhi,_<br/>
_par kyun zaahir zamane mein sab apne raqeeb hain._

> You live in every breath of mine, you rule my words of prayer. <br/>But why do I find everyone in manifest world against us.

_jis pe latak ke hum kaafir na-ummeed hue,_<br/>
_ae majboor sanam, yeh tere ishq ki sleeb hai._

> That thing which made me being classifed as an unbeliever,<br/> and on which I hang  hopelessly, it is the same cross(crucifix) of 'my  love for you'

_khwaab tak mein bhi tum tak pahunch na payein,_<br/>
_kahaan se likhwaya tune aisa naseeb hai._

> I can't even reach you in my dreams, <br/>from where did you get written , this damn fate

_Ek tu hi to thi jise humne daulat maana,_<br/>
_tere bagair dekh kitne gareeb hain._

>It was only you, whom I regarded as 'wealth', <br/>
just see without you, how poor I am
