---
title: Kalam Di Rooh
subtitle: The Soul of My Pen
date: 2014-06-20 
tags: ["poem", "punjabi"]
---

<!--more-->

>ਮੇਰੀ ਕਲਮ ਦੀ ਰੂਹ ਕਿਥੇ ਗਈ |<br/>
>ਹਰਫ਼ ਲਿਖਵਾਣ ਵਾਲੀਏ ਤੂ ਕਿਥੇ ਗਈ |

>**meri kalam di rooh kithe gyi**

_(O the spirit of my pen, where have thou gone?)_

>**_harf likhwaan waliye tu kithe gyi_**

_(the one who  made me write poems, where have thou gone)_
(ਮਾਤਰਾ = 16/20)

>ਮੁੱਕੀ ਕਲਮ        ਦੀ ਸਿਯਾਹੀ ਵੇ |<br/>
>ਦਿੰਦੀ  ਬਿਰਹਾ      ਦੀ ਗਵਾਹੀ ਵੇ |

>**mukki qalam di siyaahi ve**

_(The ink has almost dried)_

> **dendi birhaa di gwaahi ve**

_(It is the witness of our separation and its pain)_
(ਮਾਤਰਾ = 16/16)

>ਖਾਲੀ ਕਾਗਜ਼ ਜਾਵੇ  ਉੱਡ ਦਾ ਵੇ |<br/>
>ਹੁਣ ਲਫਜ਼ ਨਾ ਕੋਈ ਜੁੜ ਦਾ ਵੇ |

>**khali kaaghaz jaave udd da ve**

_(The blank paper keeps flying out of my hands)_

>**hun lafz na koi   judd da ve**

_(And  I am not able to join words and write poem)_
(ਮਾਤਰਾ = 16/16)

>ਹੁਣ ਦੀਦ ਤੇਰੀ ਦੀ ਆਰਜ਼ੂ ਕਿਥੇ ਗਈ |<br/>
>ਖਵਾਬੀਂ  ਆਣ ਵਾਲੀਏ ਤੂ ਕਿਥੇ ਗਈ |

>**hun deed teri di arzoo kithey gyi**

_(Where has desire of  having your vision gone? )_

>**khwaabee(n)  aan waliye tu kithey gyi**

_(O the dweller in my dreams,where have thou gone )_
(ਮਾਤਰਾ = 16/20)

>ਤੜਪਦੀ ਕਲਮ ਤੈਥੋਂ ਬਿਨਾ ਸੀ |<br/>
>ਉਡੀਕਦੀ       ਤੈਨੂੰ  ਕਿੰਨਾ ਸੀ |

>**tadapdi kalam tethon bina c**

_(My pen was so restless, when you were absent, to write about you)_

>**udeekdi tenu kinna c**


_(How much it used to wait for you?)_
(ਮਾਤਰਾ = 16/16)

>ਆਵਾਜ਼ ਦਿਲੋਂ   ਜੋ ਆਂਦੀ ਸੀ |<br/>
>ਓਹੀ ਨਜ਼ਮ  ਬਣੀ  ਜਾਂਦੀ ਸੀ |

>**awaaz dilo jo aandi c**

_(Whatever words came out of my heart)_

>**ohi nazam banee jandi c**

_(they used to become songs and poems)_
(ਮਾਤਰਾ = 16/16)

>ਕਿਓ ਗੁਲਸ਼ਨ ਵਿਚ ਖੁਸ਼ਬੋ   ਨਾ ਰਹੀ  |<br/>
?ਮਹਿਕ ਵਰਸਾਣ ਵਾਲੀਏ ਤੂ ਕਿਥੇ ਗਈ |

>**kyu gulshan wich khushbu na rhi,**

_(Why the garden of my heart is devoid of your fragrance)_

>**mehak warsaan waliye tu kithe gyi**

_(O the fragrant maiden, where have thou gone)_
(ਮਾਤਰਾ = 16/20)
