---
title: GABBROZEENA MUGGOZYTE
subtitle: A Funfilled Poem
date: 2012-08-02
tags: ["poem"]
---

<!--more-->

### Gabbrozeena Muggozyte
Some of my posts have strange origins, they don't usually start as poems, anecdotes or stories, they start as fun ideas, and are inspired by unusual moments. Once I and my friends - a pair of non identical twins were having a healthy laugh cracking jokes on everything normal and abnormal. In those moments we directed a few jokes on each other.

In a second my mind clicked, I remembered a joke I used to narrate during my school days, it was about a ghost. The fun part of the joke was that it conveyed that the other person(the target of mock missile) can even scare ghosts. In the evening I penned a poem narrating the story whilst adding my own flavour to it.

#### Gabbar, Mug and the Bacteria: The Making of the Protagonist
Indian movies have immortalized a villanous name 'Gabbar', as the one whose terror never lets the kids sleep in peace. With time the villian aspect of the Gabbar has eroded, he has remained as a fun name. One of the friends featuring in the last paragraph and the protagonist of the poem has been called 'Gabbar' by close cousins. :)

I took the name Gabbar, anglicized it and added a self coined name resembling a bacteria. I call this name  a scientific name, though technically it may not be one, but it sounds such to me. Thus was born a name a concatenation of Gabbar, Mug and Bacteria. Mug was added just to add impact to the name. It makes the name sound cool!!!
**Gabbrozeena Muggozyte**

#### Macavity Inspires me

In school we had an amazing poem by T.S Eliott, [Macavity](http://www.monologues.co.uk/Childrens_Favourites/Macavity.htm) the mystery cat. It had stayed with me and here my character can even scare the mystery cat away. But when our Gabrozeena goes to scare _Macavity_, as usual _*Macavity's not there.*_

#### _**Poem**_

>_Once upon a time on a lonely coast, <br/> there wandered a frantic ghost_,

>_In wild weather, on an eerie night, <br/> accompanied by her little child._

>_'Ma'ma, I am horribly scared and afraid,_<br/>
>_I am just an innocent kid,_

>_Would you mind a question, Dear?_

>_Who is the one who you ,too, fear?_<br/>
>_By whose name you ghastly swear ?_

>_Whose frantic rage, you with dread recall ?_<br/>
>_Who is the most dangerous of all ?'_

>_Mama ghost exclaimed in fright,_<br/>
> **"GABBROZEENA MUGGOZYTE"**

>_'Ma'ma, I am horribly scared and afraid,_<br/>
>_I am just an innocent kid,_

>_Who is the one who kills by a single blow,_<br/>
>_Even whose shadow _

>_About whom are the ghost legends true,_<br/>
>_Who beats the ghosts black and blue?'_

>_Mama ghost cried in fright_<br/>
 > **"GABBROZEENA MUGGOZYTE"**

>**_GABBROZEENA MUGGOZYTE_**

>_The name itself strikes fear,_<br/>
>_Hearing the name the ghosts disappear._

#### And the damage control follows

All the poem up to here presents a scary picture, a  person at mention of whom even the ghosts tremble. This is an incorrect representation of the friend. 
The truth is exactly the opposite. Who wants to mock the friends the brink of damage? Definitely not me?

Let the damage control verses flow with the same tempo :)

>_But I say, it is not right,_<br/>
>_About our dear MUGGOZYTE._

>_Whose words do make us smile,<br/>
and the one who always laughs,_

>_Who is sweet and chubby,<br/>
and knows wrath not,<br/>
ask Annu the answer she has got._

>_And in chorus we would sing in delight,<br/>
"GABBROZEENA MUGGOZYTE"._

>_Echoes would be heard in the streets tonight._

>**GABBROZEENA MUGGOZYTE**_
