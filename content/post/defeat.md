---
title: Defeat my Mind, Defeat my body
subtitle: Don't give up lightly
date: 2013-11-19 
tags: ["kabir", "spiritual", "motivation"]
---

An anecdote and a verse of Saint [Kabir](https://en.wikipedia.org/wiki/Kabir).

<!--more-->

Kabeer is one of the India's favorite mystics. He, with his words and thoughts kept challenging the monopoly of the stereotype existent in the people of those times. His verses present him as an epitome of humility who doesn't shy from mocking the things he finds useless. He puts his opinion with a clarity but laces his expression with a plethora of metaphors and figures of speech. Thus it is easy to understnd what he disagrees but it takes an effort to find out what he says and means.

He in his poems talks to his mind and his words have a penetrability, they reverberate in the readers mind and cleanse it. I would elaborate my point with the translation of one of his poems.

Kabeer could easily offend the sensitive religious and administrative heavy weights and he was not tolerated by some people fr his views and songs. They complained about him to the King. A decision was taken to throw him in to the river. He was chained and thrown into the water. But while in water, he pulled his hands out and feet out of the chains and swam to the shore. People were taken aback to see him alive.

A [verse](https://igranth.com/shabad?id=6562&tuk=49747&q=gklm) in SGGS narrates the story thus.

> __When you cannot defeat my mind, how can the fear affect my body?__ <br/>
> __I had stilled my mind on the Lotus feet of God.__

> Ganges, the holy river, is profound and deep. <br/>
> Chained and fettered, they brought Kabeer there.

> Those waves of Ganges, opened my chains, <br/>
> I was, as if, floating on a deer skin.

> Says Kabeer, when no one is with you to help, <br/>
> Raghunath will protect you on the Earth and Water.

[verse, SGGS 1162]

Kabir has a special relation of love with the cosmos and the Providence and he calls it Raam. The real thing which saved Kabir, was in his own words, the grace of God. Talking in our perspective, it was the positivism in the thoughts which he had maintained. He says you cannot terrorize my mind, since I have the support of someone stronger than you.

When mind does not entertain the fear, the body becomes strong. Most of our fears are hypothetical and not even genuine. Even if the fears and the problems are genuine, our attitude can still make a difference.

When we are not ready to accept defeat, we cannot be defeated. We are defeated when we acknowledge that we are. I would like to end it as
Ernest Hemingway puts it "A man can be destroyed but not defeated."[^1]

[^1]: https://en.wikipedia.org/wiki/The_Old_Man_and_the_Sea

