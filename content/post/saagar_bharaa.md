---
title: Ulfat
subtitle: A small poem
date: 2014-06-20 
tags: ["poem", "urdu"]
---

<!--more-->

## Ulfat


> paas saagar bhara hai lafzo ka,<br/>
> kuwwat bhi hai, izteraab bhi.

>> पास सागर भरा है लफ्ज़ो का,<br/> क़ुव्वत भी है इज़्तेराब भी। 


_(I have an ocean full of words, for your praise, <br/> my pen possesses the power/ability and mind has the needed restlessness/anxiety)_

> ke humara ulfat ki numaish ka,<br/>
> junoon bhi hai, khwaab bhi.

>> कि हमारा उल्फत की नुमाइश का,<br/> जुनून भी है, ख्वाब भी। 

_(To express my love in the words, <br/> I have enough dreams and the required passion)_

> In sehme deedo mein ashqo ka,<br/>
> shikwa bhi hai, sailaab bhi.

>> इन सहमे दीदों में अश्कों का,<br/> शिकवा भी है सैलाब भी। 

_(In these wet eyes, you will find complain/anguish <br/> and a flood of emotions as well)_

> Gwaah hai falak bhara sitaron ka,<br/>
> Aftaab bhi hai, mahtaab bhi.

>> गवाह है फलक भरा सितारों का, <br/>आफताब भी है महताब भी। 

_(The sky full of stars, <br/> along with the sun and the moon are present as witnesses for this)_

> bas teri nigaah ke pyaase hain<br/>
> warna paimaana bhi hai, shraab bhi

>> बस  तेरी निगाह के प्यासे हैं, <br/> वरना पैमाना भी है, शराब भी। 

_(I just long for your glance, I am thirsty for that only,<br/> otherwise I have a glass and wine with me as well)_


