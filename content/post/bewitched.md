---
title: Bewitched
subtitle: A light and emotional verse
date: 2010-02-14 
tags: ["poem"]
---

<!--more-->

## **Bewitched** 

_A naive tuft of rose, <br/> In a gleamy morn clear_ <br/>
_Secretly whispered to me,<br/> My heart did hear._ <br/>

_What to say dear?? <br/> I'll lose my being, I fear._

_A **Maiden** sublime <br/> In a black cascade(apparel)_ 

_With a smile so sharp <br/> as a deadly blade,_</br>

_From Valley of Beauty,_ <br/>_she kisses the Moon,_<br/> 
_Every glimpse of her face,_<br/>_enchants me to swoon._<br/>

_Resides in my fantasies,_ <br/>_that picture so clear._

_What to say dear??_ <br/>
_I'll lose my being,I fear._<br/>

_Who knew she wud hence <br/> Dis. Dis. Disappear_ <br/> 
_Would be so far, <br/>And still so near._

_What to say dear??_ <br/>
_I'll lose my being, I fear._

### Background

It was written in 2010 on insistence of a friend who wanted to gift a
poem to someone.
