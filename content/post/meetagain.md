---
title: When Shall We Meet Again
subtitle: A poem
date: 2012-07-20 
tags: ["poem", "Mystie Mystic and the Master", ""]
---

<!--more-->

# _When Shall We Meet Again_

## Abstract

This is a poem of _hopes_, the _hopes_ which strive under all circumstances. They may lie dormant for ages, cocooned
in a sheath of silken patience and spring back to life, from oblivion, as mature _Faith_.

We have three characters here:

**A Mystic**
 
> A dreamer who can weave a world of fantasies in seconds 
and reside in a world therein. 
In real world, he is specially assisted by the **_Mother Nature_.** 
He gets everything he wishes, with an effort far less 
than that is required and less than that he is capable of.
 
> Things come to him, ideas flow to him, as if 
the Fate has a special liking for him.
He loves **_The Master_** for this. 
**The Master** is an important part in his imagination. 
The Mystic calls the personification of
**_The Fate_** or **_The Grace of Nature_** as the **_Master_**.
 
> In all his poems and songs, there are odes to _The Master_. 
In his mental fantasy land, there is emotional love
brewing for _the Master_
 

**Mistie**

> A damsel going through harsh phases of life, 
who finds no place and time to express herself. 
Once she had ambitions, now all she has is, 
is a life under a social watch, which feels like a trap and a jail.
She usually spends her time and energy in complains and sobs. 
In her case **_Mother Nature_** conspires to keep her sad.
She is lonely and away from her Family.

> Things go away from her, ideas shut their door to her, 
as if the Fate rejoices in her troubles.

> She build mental fantasies of a good life, all her sobs and weeps are prayers to _the Master_. All she wants and waits for is,
the deliverance.

**Master**

>The personification of _The Fate_. Being the object of love for both __the Master__ and __Mystie__, he is the only connection
between the otherwise contrasting individuals. (We call it _Master_ or _Grace of God_)

Mystie and Mystic are living in their respective realms and have not been in contact for years. The Mind of Mystie has a question "When Shall We Meet Again". The Mystic has no answer to this question, still he thinks that they may never meet again in reality. Hence Imagination is the only place where they have a chance to meet. 

The poem sums the answer of the Mystic to the question.

---

>*When the bright spring <br/>
relinquishes its wintery shadow, <br/> 
And the sprouts of beauty <br/>
emerge to envelope the meadow.* 

>*When the Grace will fill <br/>
the void of your Life, <br/>
And a smile adorns your face <br/> 
with a resurgent glow.*

>*When my penances, would <br/>
attach to your destiny, <br/>
And strip away from your cloak, <br/> 
the thorns of pain,*

>**_Then Shall We Meet Again_**

>*When a breeze across your face, <br/>
will move dry, <br/>
And find no more tears <br/>
to wipe off again.*

>*When ghosts of the past <br/>
would no more haunt, <br/>
Our misunderstandings level <br/> 
and scorns do wane.*

>*When the plan of the Master <br/>
will come of age, <br/>
And Mistie, a free Bird, <br/>
flies out of the cage.* 

>*When a happy maiden, <br/>
enamouringly sweet and cute, <br/> 
In the innocence prime, <br/>
will dance in the rain.* <br/>

>**_Then Shall We Meet Again_**

>*When Mistie and the Mystic <br/>
will reappear, <br/>
In a Story, <br/>
Singing of the Master’s grace.*

>*When my clear words <br/> 
will come out to be true, <br/>
And there would be awe <br/>
at the blossom’s brow.*

>*When the dreams I wish to live, <br/>
may manifest, <br/>
And the Mystic’s words <br/>
dart from cupid’s bow.*

>*When the fog of duality <br/>
would, in love, subside, <br/>
And Mistie and the Mystic <br/>
appear to be same.*

>*When all the commotions of mind <br/>
are lulled to rest <br/>
And a glimpse of someone <br/>
will cut away the pain,*

>**_Then Shall We Meet Again_**

>*When Mistie and Mystic will sing <br/>
of the Master together, <br/>
In the fantastical dreams <br/>
of the damned poet insane.*


>**_Hence shall we meet again, <br/>
not to be separated again._**

---
